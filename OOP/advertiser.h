#ifndef _Advertiser_h_
#define _Advertiser_h_
#include "base_advertising.h"

class Advertiser : public BaseAdvertising
{
private:
    std::string name;
    static std::string description;
    static std::vector <BaseAdvertising*> members;
public:
    Advertiser(int, std::string);
    std::string getName();
    void setName(std::string);
    static std::string help();
    static int getTotalClicks();
    static std::string describeMe();
};

#endif
