#include <iostream>
#include <vector>
#include <fstream>
#include "advertiser.h"

std::string Advertiser::description = "The Advertiser class represents an advertiser in the online advertising system. It contains information about the advertiser’s name, clicks, and views.";
std::vector <BaseAdvertising*> Advertiser::members;

Advertiser::Advertiser(int id, std::string name)
{
    this->id = id;
    this->name = name;
    members.push_back(this);
}
std::string Advertiser::getName()
{
    return this->name;
}
void Advertiser::setName(std::string name)
{
    this->name = name;
}
std::string Advertiser::help()
{
    std::string helpText, line;
    std::ifstream file;
    file.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    try
    {
        file.open("advertiser_help.txt");
        while (std::getline(file, line))
            helpText += line + "\n";
        file.close();
    }
    catch (const std::ifstream::failure e)
    {
        std::cout << "An IOError occurred: " << e.what() << std::endl;
    }
    return helpText;
}
int Advertiser::getTotalClicks()
{
    int sum = 0;
    for (auto itr : members)
        sum += itr->getClicks();
    return sum;
}
std::string Advertiser::describeMe()
{
    return description;
}
