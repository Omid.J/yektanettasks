#include <iostream>
#include <vector>
#include "base_advertising.h"

std::string BaseAdvertising::description = "The BaseAdvertising class is the parent class of Advertiser and Ad. It contains common fields and methods for both classes, including clicks and views.";

BaseAdvertising::BaseAdvertising()
{
    this->clicks = 0;
    this->views = 0;
}
int BaseAdvertising::getClicks()
{
    return this->clicks;
}
int BaseAdvertising::getViews()
{
    return this->views;
}
void BaseAdvertising::incClicks()
{
    this->clicks++;
}
void BaseAdvertising::incViews()
{
    this->views++;
}
std::string BaseAdvertising::describeMe()
{
    return description;
}
