#include <iostream>
#include "ad.h"
#include "advertiser.h"

std::string description = "The Ad class represents an ad in the online advertising system. It contains information about the advertiser that owns the ad, image of ad, link to the ad, clicks, and views.";

Ad::Ad(int id, std::string title, std::string imgUrl, std::string link, Advertiser& advertiser)
{
    this->id = id;
    this->title = title;
    this->imgUrl = imgUrl;
    this->link = link;
    this->advertiser = &advertiser;
}
std::string Ad::getTitle()
{
    return this->title;
}
void Ad::setTitle(std::string title)
{
    this->title = title;
}
std::string Ad::getImgUrl()
{
    return this->imgUrl;
}
void Ad::setImgUrl(std::string imgUrl)
{
    this->imgUrl = imgUrl;
}
std::string Ad::getLink()
{
    return this->link;
}
void Ad::setLink(std::string link)
{
    this->link = link;
}
void Ad::setAdvertiser(Advertiser& advertiser)
{
    this->advertiser = &advertiser;
}
std::string Ad::describeMe()
{
    return "Ad description.";
}
void Ad::incClicks()
{
    this->clicks++;
    this->advertiser->incClicks();
}
void Ad::incViews()
{
    this->views++;
    this->advertiser->incViews();
}
