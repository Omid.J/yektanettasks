#ifndef _Ad_h_
#define _Ad_h_
#include "base_advertising.h"
#include "advertiser.h"

class Ad : public BaseAdvertising
{
private:
    std::string title, imgUrl, link;
    Advertiser* advertiser;
    static std::string description;
public:
    Ad(int, std::string, std::string, std::string, Advertiser&);
    std::string getTitle();
    void setTitle(std::string);
    std::string getImgUrl();
    void setImgUrl(std::string);
    std::string getLink();
    void setLink(std::string);
    void setAdvertiser(Advertiser &);
    static std::string describeMe();
    void incClicks();
    void incViews();
};

#endif
