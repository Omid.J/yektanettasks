#ifndef _Base_Advertising_h_
#define _Base_Advertising_h_

class BaseAdvertising
{
private:
    static std::string description;
protected:
    int id;
    int clicks, views;
public:
    BaseAdvertising();
    int getClicks();
    int getViews();
    void incClicks();
    void incViews();
    static std::string describeMe();
};

#endif
